package ch.cimnine.siebesiech.channels

import android.net.Uri
import ch.cimnine.siebesiech.channels.ChannelList.Category.*
import java.io.Serializable
import java.util.*

/**
 * Helper class for providing TV channels
 */
@Suppress("UNUSED_CHANGED_VALUE")
object ChannelList {
    /**
     * An array of channels.
     */
    val ITEMS: MutableList<Channel> = ArrayList()

    /**
     * A map of channels by ID.
     */
    val ITEM_MAP: MutableMap<String, Channel> = HashMap()

    /**
     * A map of channels by category
     */
    val CATEGORY_MAP: MutableMap<Category, MutableList<Channel>> = HashMap()

    enum class Category(val stringId: Int) {
        DE(R.string.de),
        FR(R.string.fr),
        IT(R.string.it),
        EN(R.string.en),
        OTHER(R.string.other)
    }

    init {
        var i = 1
        addItem(Channel((i++).toString(), "SRF1 HD", "udp://@239.77.0.77:5000", DE, R.drawable.ic_logo_srf_1))
        addItem(Channel((i++).toString(), "SRFzwei HD", "udp://@239.77.0.78:5000", DE, R.drawable.ic_logo_srf_2))
        addItem(Channel((i++).toString(), "SRF Info HD", "udp://@239.77.0.5:5000", DE, R.drawable.ic_logo_srf_info))
        addItem(Channel((i++).toString(), "3+ HD", "udp://@239.77.0.117:5000", DE))
        addItem(Channel((i++).toString(), "4+ HD", "udp://@239.77.0.118:5000", DE))
        addItem(Channel((i++).toString(), "5+ HD", "udp://@239.77.0.179:5000", DE))
        addItem(Channel((i++).toString(), "TV24 HD", "udp://@239.77.0.183:5000", DE))
        addItem(Channel((i++).toString(), "TV25 HD", "udp://@239.77.0.184:5000", DE))
        addItem(Channel((i++).toString(), "Tele Züri HD", "udp://@239.77.0.203:5000", DE))
        addItem(Channel((i++).toString(), "TELE TOP HD", "udp://@239.77.0.206:5000", DE))
        addItem(Channel((i++).toString(), "TeleBärn HD", "udp://@239.77.0.201:5000", DE))
        addItem(Channel((i++).toString(), "TVO HD", "udp://@239.77.0.209:5000", DE))
        addItem(Channel((i++).toString(), "Tele M1 HD", "udp://@239.77.0.214:5000", DE))
        addItem(Channel((i++).toString(), "TeleBasel HD", "udp://@239.77.0.202:5000", DE))
        addItem(Channel((i++).toString(), "Tele Z HD", "udp://@239.77.0.213:5000", DE))
        addItem(Channel((i++).toString(), "Tele1 HD", "udp://@239.77.0.204:5000", DE))
        addItem(Channel((i++).toString(), "TV Südostschweiz HD", "udp://@239.77.0.222:5000", DE))
        addItem(Channel((i++).toString(), "Tele Bielingue HD", "udp://@239.77.1.42:5000", DE))
        addItem(Channel((i++).toString(), "Star TV HD", "udp://@239.77.0.191:5000", DE))
        addItem(Channel((i++).toString(), "Puls 8 HD", "udp://@239.77.0.180:5000", DE))
        addItem(Channel((i++).toString(), "CHTV HD", "udp://@239.77.0.181:5000", DE))
        addItem(Channel((i++).toString(), "Schweiz 5", "udp://@239.77.0.21:5000", DE))
        addItem(Channel((i++).toString(), "TeleD HD", "udp://@239.77.0.208:5000", DE))
        addItem(Channel((i++).toString(), "Alf", "udp://@239.77.5.154:5000", DE))
        addItem(Channel((i++).toString(), "S1 HD", "udp://@239.77.0.217:5000", DE))
        addItem(Channel((i++).toString(), "SWISS1 HD", "udp://@239.77.0.76:5000", DE))
        addItem(Channel((i++).toString(), "wetter.tv", "udp://@239.77.0.224:5000", DE))
        addItem(Channel((i++).toString(), "Teleclub Zoom", "udp://@239.77.0.4:5000", DE))
        addItem(Channel((i++).toString(), "MySports HD D", "udp://@239.77.0.12:5000", DE))
        addItem(Channel((i++).toString(), "Arte HD", "udp://@239.77.0.82:5000", DE))
        addItem(Channel((i++).toString(), "3Sat HD", "udp://@239.77.0.106:5000", DE))
        addItem(Channel((i++).toString(), "ORF1 HD", "udp://@239.77.0.109:5000", DE))
        addItem(Channel((i++).toString(), "ORF2 HD", "udp://@239.77.0.110:5000", DE))
        addItem(Channel((i++).toString(), "Servus TV HD", "udp://@239.77.0.84:5000", DE))
        addItem(Channel((i++).toString(), "Das Erste HD", "udp://@239.77.0.80:5000", DE))
        addItem(Channel((i++).toString(), "ZDF HD", "udp://@239.77.0.79:5000", DE))
        addItem(Channel((i++).toString(), "RTL CH", "udp://@239.77.0.190:5000", DE))
        addItem(Channel((i++).toString(), "RTL 2 (Schweiz)", "udp://@239.77.0.14:5000", DE))
        addItem(Channel((i++).toString(), "Super RTL (Schweiz)", "udp://@239.77.0.29:5000", DE))
        addItem(Channel((i++).toString(), "RTL Nitro", "udp://@239.77.0.88:5000", DE))
        addItem(Channel((i++).toString(), "ProSieben (Schweiz)", "udp://@239.77.0.15:5000", DE))
        addItem(Channel((i++).toString(), "ProSieben Maxx", "udp://@239.77.0.119:5000", DE))
        addItem(Channel((i++).toString(), "Sat.1 (Schweiz)", "udp://@239.77.0.11:5000", DE))
        addItem(Channel((i++).toString(), "Sat1 Gold", "udp://@239.77.0.99:5000", DE))
        addItem(Channel((i++).toString(), "VOX (Schweiz)", "udp://@239.77.0.17:5000", DE))
        addItem(Channel((i++).toString(), "SIXX", "udp://@239.77.0.69:5000", DE))
        addItem(Channel((i++).toString(), "Kabel1 (Schweiz)", "udp://@239.77.0.18:5000", DE))
        addItem(Channel((i++).toString(), "Kika HD", "udp://@239.77.0.108:5000", DE))
        addItem(Channel((i++).toString(), "Nickelodeon CH HD", "udp://@239.77.0.197:5000", DE))
        addItem(Channel((i++).toString(), "Disney Channel", "udp://@239.77.0.44:5000", DE))
        addItem(Channel((i++).toString(), "VIVA CH HD", "udp://@239.77.0.192:5000", DE))
        addItem(Channel((i++).toString(), "MTV Schweiz HD", "udp://@239.77.0.196:5000", DE))
        addItem(Channel((i++).toString(), "Eurosport", "udp://@239.77.0.34:5000", DE))
        addItem(Channel((i++).toString(), "SPORT1 HD CH", "udp://@239.77.0.193:5000", DE))
        addItem(Channel((i++).toString(), "DMAX HD", "udp://@239.77.0.195:5000", DE))
        addItem(Channel((i++).toString(), "TLC", "udp://@239.77.0.132:5000", DE))
        addItem(Channel((i++).toString(), "Anixe HD", "udp://@239.77.0.83:5000", DE))
        addItem(Channel((i++).toString(), "one HD", "udp://@239.77.0.111:5000", DE))
        addItem(Channel((i++).toString(), "ZDF Info HD", "udp://@239.77.0.102:5000", DE))
        addItem(Channel((i++).toString(), "ZDF Neo HD", "udp://@239.77.0.100:5000", DE))
        addItem(Channel((i++).toString(), "Welt der Wunder", "udp://@239.77.0.46:5000", DE))
        addItem(Channel((i++).toString(), "Welt HD", "udp://@239.77.0.194:5000", DE))
        addItem(Channel((i++).toString(), "ARD Alpha", "udp://@239.77.0.51:5000", DE))
        addItem(Channel((i++).toString(), "Phoenix HD", "udp://@239.77.0.105:5000", DE))
        addItem(Channel((i++).toString(), "tagesschau24 HD", "udp://@239.77.0.129:5000", DE))
        addItem(Channel((i++).toString(), "n-tv", "udp://@239.77.0.24:5000", DE))
        addItem(Channel((i++).toString(), "EuroNews", "udp://@239.77.0.43:5000", DE))
        addItem(Channel((i++).toString(), "DW Europe", "udp://@239.77.0.66:5000", DE))
        addItem(Channel((i++).toString(), "SWR BW HD", "udp://@239.77.0.81:5000", DE))
        addItem(Channel((i++).toString(), "BR HD", "udp://@239.77.0.107:5000", DE))
        addItem(Channel((i++).toString(), "WDR HD Köln", "udp://@239.77.0.215:5000", DE))
        addItem(Channel((i++).toString(), "NDR HD", "udp://@239.77.0.103:5000", DE))
        addItem(Channel((i++).toString(), "MDR Sachsen HD", "udp://@239.77.0.124:5000", DE))
        addItem(Channel((i++).toString(), "rbb Berlin HD", "udp://@239.77.0.123:5000", DE))
        addItem(Channel((i++).toString(), "hr-fernsehen HD", "udp://@239.77.0.122:5000", DE))
        addItem(Channel((i++).toString(), "Tele 5", "udp://@239.77.0.64:5000", DE))
        addItem(Channel((i++).toString(), "Die Neue Zeit TV", "udp://@239.77.0.60:5000", DE))
        addItem(Channel((i++).toString(), "Bibel TV HD", "udp://@239.77.0.127:5000", OTHER))
        addItem(Channel((i++).toString(), "HSE24 ", "udp://@239.77.0.63:5000", OTHER))
        addItem(Channel((i++).toString(), "HSE24 TREND", "udp://@239.77.0.216:5000", OTHER))
        addItem(Channel((i++).toString(), "Channel 55", "udp://@239.77.0.223:5000", OTHER))
        addItem(Channel((i++).toString(), "GoTV", "udp://@239.77.0.97:5000", OTHER))
        addItem(Channel((i++).toString(), "Booster TV", "udp://@239.77.0.225:5000", OTHER))
        addItem(Channel((i++).toString(), "RTS 1 HD", "udp://@239.77.1.28:5000", FR))
        addItem(Channel((i++).toString(), "RTS 2 HD", "udp://@239.77.1.29:5000", FR))
        addItem(Channel((i++).toString(), "la télé HD", "udp://@239.77.1.39:5000", FR))
        addItem(Channel((i++).toString(), "Canal Alpha JU HD", "udp://@239.77.1.53:5000", FR))
        addItem(Channel((i++).toString(), "Canal Alpha HD", "udp://@239.77.1.37:5000", FR))
        addItem(Channel((i++).toString(), "Télé Versoix", "udp://@239.77.1.55:5000", FR))
        addItem(Channel((i++).toString(), "MySports HD F", "udp://@239.77.1.56:5000", FR))
        addItem(Channel((i++).toString(), "TF1 HD", "udp://@239.77.1.3:5000", FR))
        addItem(Channel((i++).toString(), "France 2 HD", "udp://@239.77.1.4:5000", FR))
        addItem(Channel((i++).toString(), "France 3", "udp://@239.77.1.5:5000", FR))
        addItem(Channel((i++).toString(), "France 4", "udp://@239.77.1.6:5000", FR))
        addItem(Channel((i++).toString(), "France 5", "udp://@239.77.1.7:5000", FR))
        addItem(Channel((i++).toString(), "M6 HD", "udp://@239.77.1.10:5000", FR))
        addItem(Channel((i++).toString(), "C8 HD CH", "udp://@239.77.1.54:5000", FR))
        addItem(Channel((i++).toString(), "W9 HD", "udp://@239.77.1.13:5000", FR))
        addItem(Channel((i++).toString(), "RTL 9 HD", "udp://@239.77.1.36:5000", FR))
        addItem(Channel((i++).toString(), "NRJ12", "udp://@239.77.1.16:5000", FR))
        addItem(Channel((i++).toString(), "Arte F HD", "udp://@239.77.1.47:5000", FR))
        addItem(Channel((i++).toString(), "NT1", "udp://@239.77.1.20:5000", FR))
        addItem(Channel((i++).toString(), "Gulli", "udp://@239.77.1.23:5000", FR))
        addItem(Channel((i++).toString(), "TMC", "udp://@239.77.1.15:5000", FR))
        addItem(Channel((i++).toString(), "BFM TV", "udp://@239.77.1.24:5000", FR))
        addItem(Channel((i++).toString(), "TVM3", "udp://@239.77.1.38:5000", FR))
        addItem(Channel((i++).toString(), "Rouge TV HD", "udp://@239.77.1.40:5000", FR))
        addItem(Channel((i++).toString(), "6ter", "udp://@239.77.1.46:5000", FR))
        addItem(Channel((i++).toString(), "2M Maroc", "udp://@239.77.1.45:5000", FR))
        addItem(Channel((i++).toString(), "Numéro 23", "udp://@239.77.1.50:5000", FR))
        addItem(Channel((i++).toString(), "LFM TV", "udp://@239.77.1.51:5000", FR))
        addItem(Channel((i++).toString(), "One TV", "udp://@239.77.1.52:5000", FR))
        addItem(Channel((i++).toString(), "Euronews F", "udp://@239.77.1.41:5000", FR))
        addItem(Channel((i++).toString(), "France 24", "udp://@239.77.1.43:5000", FR))
        addItem(Channel((i++).toString(), "TV5MONDE EUROPE", "udp://@239.77.1.49:5000", FR))
        addItem(Channel((i++).toString(), "Alsace 20", "udp://@239.77.1.25:5000", FR))
        addItem(Channel((i++).toString(), "RSI LA 1 HD", "udp://@239.77.2.17:5000", IT))
        addItem(Channel((i++).toString(), "RSI LA 2 HD", "udp://@239.77.2.18:5000", IT))
        addItem(Channel((i++).toString(), "Tele Ticino HD", "udp://@239.77.2.73:5000", IT))
        addItem(Channel((i++).toString(), "Rai HD", "udp://@239.77.2.19:5000", IT))
        addItem(Channel((i++).toString(), "Rai Uno", "udp://@239.77.2.3:5000", IT))
        addItem(Channel((i++).toString(), "Rai Due", "udp://@239.77.2.4:5000", IT))
        addItem(Channel((i++).toString(), "Rai Tre", "udp://@239.77.2.5:5000", IT))
        addItem(Channel((i++).toString(), "Rai 4", "udp://@239.77.2.6:5000", IT))
        addItem(Channel((i++).toString(), "Rete4 HD", "udp://@239.77.2.21:5000", IT))
        addItem(Channel((i++).toString(), "Canale5 HD", "udp://@239.77.2.22:5000", IT))
        addItem(Channel((i++).toString(), "Italia 1 HD", "udp://@239.77.2.20:5000", IT))
        addItem(Channel((i++).toString(), "LA 7", "udp://@239.77.2.15:5000", IT))
        addItem(Channel((i++).toString(), "LA7d", "udp://@239.77.2.12:5000", IT))
        addItem(Channel((i++).toString(), "IRIS", "udp://@239.77.2.14:5000", IT))
        addItem(Channel((i++).toString(), "Boing", "udp://@239.77.2.16:5000", IT))
        addItem(Channel((i++).toString(), "Rai Sport+ HD", "udp://@239.77.2.10:5000", IT))
        addItem(Channel((i++).toString(), "Top Calcio 24", "udp://@239.77.2.52:5000", IT))
        addItem(Channel((i++).toString(), "Sportitalia", "udp://@239.77.2.64:5000", IT))
        addItem(Channel((i++).toString(), "Sportitalia 2", "udp://@239.77.2.68:5000", IT))
        addItem(Channel((i++).toString(), "Sportitalia 24", "udp://@239.77.2.65:5000", IT))
        addItem(Channel((i++).toString(), "Rai Storia", "udp://@239.77.2.31:5000", IT))
        addItem(Channel((i++).toString(), "Rai News ", "udp://@239.77.2.32:5000", IT))
        addItem(Channel((i++).toString(), "Rai Premium", "udp://@239.77.2.33:5000", IT))
        addItem(Channel((i++).toString(), "Rai Yoyo", "udp://@239.77.2.34:5000", IT))
        addItem(Channel((i++).toString(), "Rai Movie", "udp://@239.77.2.36:5000", IT))
        addItem(Channel((i++).toString(), "VH1", "udp://@239.77.2.13:5000", IT))
        addItem(Channel((i++).toString(), "Real Time", "udp://@239.77.2.63:5000", IT))
        addItem(Channel((i++).toString(), "TELELOMBARDIA HD", "udp://@239.77.2.23:5000", IT))
        addItem(Channel((i++).toString(), "ANTENNATRE HD", "udp://@239.77.2.24:5000", IT))
        addItem(Channel((i++).toString(), "BBC One HD", "udp://@239.77.3.30:5000", EN))
        addItem(Channel((i++).toString(), "BBC Two HD", "udp://@239.77.3.29:5000", EN))
        addItem(Channel((i++).toString(), "ITV 1 HD", "udp://@239.77.3.31:5000", EN))
        addItem(Channel((i++).toString(), "Channel 4 HD", "udp://@239.77.3.32:5000", EN))
        addItem(Channel((i++).toString(), "CBBC HD", "udp://@239.77.3.54:5000", EN))
        addItem(Channel((i++).toString(), "CBeebies HD", "udp://@239.77.3.55:5000", EN))
        addItem(Channel((i++).toString(), "CITV", "udp://@239.77.3.17:5000", EN))
        addItem(Channel((i++).toString(), "ITV 2", "udp://@239.77.3.14:5000", EN))
        addItem(Channel((i++).toString(), "ITV 3", "udp://@239.77.3.15:5000", EN))
        addItem(Channel((i++).toString(), "ITV 4", "udp://@239.77.3.16:5000", EN))
        addItem(Channel((i++).toString(), "BBC Four HD", "udp://@239.77.3.62:5000", EN))
        addItem(Channel((i++).toString(), "E4", "udp://@239.77.3.8:5000", EN))
        addItem(Channel((i++).toString(), "Film 4", "udp://@239.77.3.21:5000", EN))
        addItem(Channel((i++).toString(), "BBC World News Europe HD", "udp://@239.77.3.53:5000", EN))
        addItem(Channel((i++).toString(), "BBC NEWS HD", "udp://@239.77.3.52:5000", EN))
        addItem(Channel((i++).toString(), "CNN International", "udp://@239.77.3.23:5000", EN))
        addItem(Channel((i++).toString(), "CNBC Europe", "udp://@239.77.3.26:5000", EN))
        addItem(Channel((i++).toString(), "Bloomberg", "udp://@239.77.3.27:5000", EN))
        addItem(Channel((i++).toString(), "Sky News", "udp://@239.77.3.25:5000", EN))
        addItem(Channel((i++).toString(), "Russia Today", "udp://@239.77.5.51:5000", EN))
        addItem(Channel((i++).toString(), "Al Jazeera English HD", "udp://@239.77.5.153:5000", EN))
        addItem(Channel((i++).toString(), "France 24 E", "udp://@239.77.1.44:5000", EN))
        addItem(Channel((i++).toString(), "5 USA", "udp://@239.77.3.59:5000", EN))
        addItem(Channel((i++).toString(), "5STAR", "udp://@239.77.3.60:5000", EN))
        addItem(Channel((i++).toString(), "Travel", "udp://@239.77.3.50:5000", EN))
        addItem(Channel((i++).toString(), "More 4", "udp://@239.77.3.9:5000", EN))
        addItem(Channel((i++).toString(), "Fashion TV", "udp://@239.77.3.28:5000", EN))
        addItem(Channel((i++).toString(), "TVR International", "udp://@239.77.5.112:5000", EN))
        addItem(Channel((i++).toString(), "TVE Internacional", "udp://@239.77.4.1:5000", OTHER))
        addItem(Channel((i++).toString(), "Canal 24 Horas", "udp://@239.77.4.2:5000", OTHER))
        addItem(Channel((i++).toString(), "RTP 1", "udp://@239.77.4.11:5000", OTHER))
        addItem(Channel((i++).toString(), "RTV Montenegro", "udp://@239.77.5.140:5000", OTHER))
        addItem(Channel((i++).toString(), "Svet Plus", "udp://@239.77.5.141:5000", OTHER))
        addItem(Channel((i++).toString(), "TVP Polonia", "udp://@239.77.5.21:5000", OTHER))
        addItem(Channel((i++).toString(), "Duna TV", "udp://@239.77.5.157:5000", OTHER))
        addItem(Channel((i++).toString(), "Atlas TV", "udp://@239.77.5.155:5000", OTHER))
        addItem(Channel((i++).toString(), "BN", "udp://@239.77.5.163:5000", OTHER))
        addItem(Channel((i++).toString(), "TV CG", "udp://@239.77.5.164:5000", OTHER))
        addItem(Channel((i++).toString(), "4E", "udp://@239.77.5.138:5000", OTHER))
        addItem(Channel((i++).toString(), "C1R Europe", "udp://@239.77.5.136:5000", OTHER))
        addItem(Channel((i++).toString(), "RTK 1", "udp://@239.77.5.38:5000", OTHER))
        addItem(Channel((i++).toString(), "TVSH", "udp://@239.77.5.167:5000", OTHER))
        addItem(Channel((i++).toString(), "TV Shqiptar", "udp://@239.77.5.142:5000", OTHER))
        addItem(Channel((i++).toString(), "RTRS plus", "udp://@239.77.5.148:5000", OTHER))
        addItem(Channel((i++).toString(), "STERK TV", "udp://@239.77.5.151:5000", OTHER))
        addItem(Channel((i++).toString(), "BVN TV", "udp://@239.77.5.18:5000", OTHER))
        addItem(Channel((i++).toString(), "Arirang World", "udp://@239.77.5.70:5000", OTHER))
        addItem(Channel((i++).toString(), "CCTV4", "udp://@239.77.5.66:5000", OTHER))
        addItem(Channel((i++).toString(), "PCNE Chinese", "udp://@239.77.5.149:5000", OTHER))
        addItem(Channel((i++).toString(), "Thai Global Network", "udp://@239.77.5.69:5000", OTHER))
        addItem(Channel((i++).toString(), "TRT TURK", "udp://@239.77.5.132:5000", OTHER))
        addItem(Channel((i++).toString(), "KANAL 7 AVRUPA", "udp://@239.77.5.131:5000", OTHER))
        addItem(Channel((i++).toString(), "HABERTURK", "udp://@239.77.5.133:5000", OTHER))
        addItem(Channel((i++).toString(), "TRT 3-SPOR", "udp://@239.77.5.130:5000", OTHER))
        addItem(Channel((i++).toString(), "FB TV", "udp://@239.77.5.135:5000", OTHER))
        addItem(Channel((i++).toString(), "TTN", "udp://@239.77.5.160:5000", OTHER))
        addItem(Channel((i++).toString(), "LBC Europe", "udp://@239.77.5.161:5000", OTHER))
        addItem(Channel((i++).toString(), "Al Masriyah", "udp://@239.77.5.165:5000", OTHER))
        addItem(Channel((i++).toString(), "Sama Dubai", "udp://@239.77.5.168:5000", OTHER))
        addItem(Channel((i++).toString(), "MBC Europe", "udp://@239.77.5.169:5000", OTHER))
        addItem(Channel((i++).toString(), "Saudi 1", "udp://@239.77.5.171:5000", OTHER))
        addItem(Channel((i++).toString(), "Dubai Sports 3", "udp://@239.77.5.172:5000", OTHER))
        addItem(Channel((i++).toString(), "Jordan TV", "udp://@239.77.5.173:5000", OTHER))
        addItem(Channel((i++).toString(), "Kuwait TV", "udp://@239.77.5.174:5000", OTHER))
        addItem(Channel((i++).toString(), "Oman TV", "udp://@239.77.5.175:5000", OTHER))
        addItem(Channel((i++).toString(), "Qatar TV", "udp://@239.77.5.176:5000", OTHER))
        addItem(Channel((i++).toString(), "Sharjah TV", "udp://@239.77.5.178:5000", OTHER))
        addItem(Channel((i++).toString(), "Dubai TV", "udp://@239.77.5.179:5000", OTHER))
        addItem(Channel((i++).toString(), "Abu Dhabi TV", "udp://@239.77.5.180:5000", OTHER))
        addItem(Channel((i++).toString(), "Al Sharqiya", "udp://@239.77.5.181:5000", OTHER))
        addItem(Channel((i++).toString(), "BBB Teststream", "udp://@239.77.0.1:5000", OTHER))
    }

    private fun addItem(item: Channel) {
        ITEMS += item
        ITEM_MAP[item.id] = item

        if (CATEGORY_MAP[item.category] == null) {
            CATEGORY_MAP[item.category] = mutableListOf(item)
        } else {
            CATEGORY_MAP[item.category] as MutableList += item
        }
    }

    /**
     * An item representing a tv channel.
     */
    data class Channel (
        val id: String,
        val name: String,
        val url: String,
        val category: Category,
        val logo: Int = R.drawable.ic_testpattern
    ) : Serializable {
        override fun toString(): String = name

        fun uri(): Uri = Uri.parse(url)
    }
}
