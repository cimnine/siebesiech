# Siebesiech

An Android application to stream TV7 by [Init7](https://www.init7.net).

## Dependencies

This project depends on the [ExoPlayer](https://github.com/google/exoplayer).
You must have it's code checked out in `../ExoPlayer`, or you need to adjust the path in the `settings.gradle` file.

## Building ffmpeg

You need exactly [Android NDK r15c][ndk15c].

`cd` into the `../ExoPlayer` directory and then run the following:

```bash
#!/bin/bash
{
EXOPLAYER_ROOT="$(pwd)"
FFMPEG_EXT_PATH="${EXOPLAYER_ROOT}/extensions/ffmpeg/src/main"
HOST_PLATFORM="darwin-x86_64"
#HOST_PLATFORM="linux-x86_64"

NDK_PATH=~/Downloads/android-ndk-r15c

echo "EXOPLAYER_ROOT=\"${EXOPLAYER_ROOT}\""
echo "FFMPEG_EXT_PATH=\"${FFMPEG_EXT_PATH}\""
echo "HOST_PLATFORM=\"${HOST_PLATFORM}\""
echo "NDK_PATH=\"${NDK_PATH}\""

COMMON_OPTIONS="\
    --target-os=android \
    --disable-static \
    --enable-shared \
    --disable-doc \
    --disable-programs \
    --disable-everything \
    --disable-avdevice \
    --disable-avformat \
    --disable-swscale \
    --disable-postproc \
    --disable-avfilter \
    --disable-symver \
    --disable-swresample \
    --enable-avresample \
    --enable-decoder=ac3 \
    --enable-decoder=mp3 \
    " && \
cd "${FFMPEG_EXT_PATH}/jni" && \
(git -C ffmpeg pull || git clone git://source.ffmpeg.org/ffmpeg ffmpeg) && \
cd ffmpeg && git checkout release/4.1 && \
make clean && ./configure \
    --libdir=android-libs/armeabi-v7a \
    --arch=arm \
    --cpu=armv7-a \
    --cross-prefix="${NDK_PATH}/toolchains/arm-linux-androideabi-4.9/prebuilt/${HOST_PLATFORM}/bin/arm-linux-androideabi-" \
    --sysroot="${NDK_PATH}/platforms/android-14/arch-arm/" \
    --extra-cflags="-march=armv7-a -mfloat-abi=softfp" \
    --extra-ldflags="-Wl,--fix-cortex-a8" \
    --extra-ldexeflags=-pie \
    ${COMMON_OPTIONS} \
    && \
make -j4 && make install-libs && \
make clean && ./configure \
    --libdir=android-libs/arm64-v8a \
    --arch=aarch64 \
    --cpu=armv8-a \
    --cross-prefix="${NDK_PATH}/toolchains/aarch64-linux-android-4.9/prebuilt/${HOST_PLATFORM}/bin/aarch64-linux-android-" \
    --sysroot="${NDK_PATH}/platforms/android-21/arch-arm64/" \
    --extra-ldexeflags=-pie \
    ${COMMON_OPTIONS} \
    && \
make -j4 && make install-libs && \
make clean && ./configure \
    --libdir=android-libs/x86 \
    --arch=x86 \
    --cpu=i686 \
    --cross-prefix="${NDK_PATH}/toolchains/x86-4.9/prebuilt/${HOST_PLATFORM}/bin/i686-linux-android-" \
    --sysroot="${NDK_PATH}/platforms/android-14/arch-x86/" \
    --extra-ldexeflags=-pie \
    --disable-asm \
    ${COMMON_OPTIONS} \
    && \
make -j4 && make install-libs && \
make clean

cd "${FFMPEG_EXT_PATH}"/jni && \
${NDK_PATH}/ndk-build APP_ABI="armeabi-v7a arm64-v8a x86" -j4
}
```

[ndk15c]: https://dl.google.com/android/repository/android-ndk-r15c-darwin-x86_64.zip

## Limitations

The current target SDK is Android 8.1, but I believe this could be lowered.

## TODOs

* Parse the channel list instead of hardcoding it
* The mobile app should only work if connected via WiFi or Ethernet
* Create a Logo
* Audio Track Selector
* Subtitles
