package ch.cimnine.siebesiech

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ch.cimnine.siebesiech.channels.ChannelList
import ch.cimnine.siebesiech.multicastsource.MulticastPlayer
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_FIT
import kotlinx.android.synthetic.main.activity_channel_detail.*
import kotlinx.android.synthetic.main.channel_detail.view.*

/**
 * A fragment representing a single Channel detail screen.
 * This fragment is either contained in a [ChannelListActivity]
 * in two-pane mode (on tablets) or a [ChannelDetailActivity]
 * on handsets.
 */
class ChannelDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private var item: ChannelList.Channel? = null
    private var multicastPlayer: MulticastPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                item = ChannelList.ITEM_MAP[it.getString(ARG_ITEM_ID)!!]
                activity?.toolbar_layout?.title = item?.name
                multicastPlayer = MulticastPlayer(context, item?.uri()!!)
                activity?.fab?.setOnClickListener { fabView ->
                    val fab = fabView as FloatingActionButton
                    if (player?.playWhenReady!!) {
                        player?.playWhenReady = false // pause
                        fab.setImageResource(android.R.drawable.ic_media_play)
                    } else {
                        player?.playWhenReady = true // play
                        fab.setImageResource(android.R.drawable.ic_media_pause)
                    }
                }
            }
        }
    }

    private var player: ExoPlayer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.channel_detail, container, false)

        player = multicastPlayer?.create() as ExoPlayer?
        item?.let {
            rootView.exo_player.player = player
            rootView.exo_player.resizeMode = RESIZE_MODE_FIT
        }

        return rootView
    }

    override fun onDetach() {
        player?.stop()

        super.onDetach()
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_ITEM_ID = "item_id"
    }
}
