package ch.cimnine.siebesiech.multicastsource

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util

class MulticastPlayer(ctx: Context?, private var uri: Uri) : Player.EventListener {
    private val trackSelector = DefaultTrackSelector()
    private val renderersFactory = DefaultRenderersFactory(ctx, DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
    private val player = ExoPlayerFactory.newSimpleInstance(ctx, renderersFactory, trackSelector)
    private val dataSourceFactory = DefaultDataSourceFactory(ctx, Util.getUserAgent(ctx, "siebesiech"))
    private val multicastDataSourceFactory = MulticastDataSourceFactory()

    fun create(): Player {
        trackSelector.parameters = trackSelector.buildUponParameters().setPreferredAudioLanguage("deu").build()

        val mediaSource = if ("udp" == uri.scheme) {
            ExtractorMediaSource.Factory(multicastDataSourceFactory).createMediaSource(uri)
        } else {
            ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
        }

        player.addListener(this)

        player.prepare(mediaSource)
        player.playWhenReady = true
        return player
    }

    override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
        val sb = StringBuilder()
        for (i in 0 until trackGroups?.length!!) {
            val formats = trackGroups.get(i)
            for (j in 0 until formats?.length!!) {
                sb.append(formats.getFormat(j).toString() + ", ")
            }
        }
        Log.d("MulticastPlayer", "Tracks: $sb")
    }

    override fun onPlayerError(error: ExoPlaybackException?) {
        when (error!!.type) {
            ExoPlaybackException.TYPE_SOURCE -> Log.e(
                "MulticastPlayer",
                "TYPE_SOURCE: ${error.sourceException.message}"
            )

            ExoPlaybackException.TYPE_RENDERER -> Log.e(
                "MulticastPlayer",
                "TYPE_RENDERER: ${error.rendererException.message}"
            )

            ExoPlaybackException.TYPE_UNEXPECTED -> Log.e(
                "MulticastPlayer",
                "TYPE_UNEXPECTED: ${error.unexpectedException.message}"
            )
        }
    }
}
