package ch.cimnine.siebesiech.multicastsource;

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.net.Uri;
import android.util.Log;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.upstream.BaseDataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * A multicast {@link com.google.android.exoplayer2.upstream.DataSource}.
 */
public class MulticastDataSource extends BaseDataSource {
    /**
     * Thrown when an error is encountered when trying to read from a {@link MulticastDataSource}.
     */
    public static final class MulticastDataSourceException extends IOException {
        MulticastDataSourceException(IOException cause) {
            super(cause);
        }
    }

    static final int DEFAULT_MAX_PACKET_SIZE = 2000;
    private final TransferListener transferListener;
    private final DatagramPacket packet;
    private DataSpec dataSpec;
    private MulticastSocket socket;
    private boolean opened;
    private byte[] packetBuffer;
    private int amountReadFromMulticast;

    public MulticastDataSource() {
        this(null);
    }

    public MulticastDataSource(TransferListener transferListener) {
        this(transferListener, DEFAULT_MAX_PACKET_SIZE);
    }

    public MulticastDataSource(TransferListener transferListener, int maxPacketSize) {
        super(true);
        this.transferListener = transferListener;
        packetBuffer = new byte[maxPacketSize];
        packet = new DatagramPacket(packetBuffer, 0, maxPacketSize);
    }

    @Override
    public long open(DataSpec dataSpec) throws MulticastDataSourceException {
        this.dataSpec = dataSpec;
        String host = dataSpec.uri.getHost();
        int port = dataSpec.uri.getPort();

        try {
            socket = new MulticastSocket(port);
            socket.joinGroup(InetAddress.getByName(host));
        } catch (IOException e) {
            throw new MulticastDataSourceException(e);
        }

        Log.d("MulticastDataSource", host + ":" + port);

        opened = true;
        if (transferListener != null) {
            transferListener.onTransferStart(this, dataSpec, true);
        }
        return C.LENGTH_UNSET;
    }

    @Override
    public void close() {
        if (opened) {
            socket.close();
            socket = null;
            amountReadFromMulticast = 0;
            opened = false;
            if (transferListener != null) {
                transferListener.onTransferEnd(this, dataSpec, true);
            }
        }
    }

    @Override
    public int read(byte[] buffer, int offset, int readLength) throws MulticastDataSourceException {
        // if we've read all the data, get another packet
        if (amountReadFromMulticast == 0) {
            try {
                socket.receive(packet);
            } catch (IOException e) {
                throw new MulticastDataSourceException(e);
            }

            amountReadFromMulticast = packet.getLength();
        }

        // don't try to read too much
        if (amountReadFromMulticast < readLength) {
            readLength = amountReadFromMulticast;
        }

        if (transferListener != null) {
            transferListener.onBytesTransferred(this, dataSpec, true, readLength);
        }

        int packetOffset = packet.getLength() - amountReadFromMulticast;
        System.arraycopy(packetBuffer, packetOffset, buffer, offset, readLength);

        amountReadFromMulticast -= readLength;
        return readLength;
    }

    @Override
    public Uri getUri() {
        return dataSpec == null ? null : dataSpec.uri;
    }
}
