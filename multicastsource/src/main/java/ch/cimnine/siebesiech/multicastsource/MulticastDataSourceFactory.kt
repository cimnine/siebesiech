package ch.cimnine.siebesiech.multicastsource

import com.google.android.exoplayer2.upstream.DataSource

class MulticastDataSourceFactory : DataSource.Factory {
    override fun createDataSource(): DataSource {
        return MulticastDataSource()
    }
}
